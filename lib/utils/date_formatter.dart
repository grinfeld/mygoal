class MyDateFormatter {
  static String getStringDuration(int time) {
    if (time == 0) {
      return "Дата цели достигнута!";
    }
    Duration duration = new Duration(milliseconds: time);
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String threeDigits(int n) {
      if (n >= 100) {
        return "$n";
      } else if (n >= 10) {
        return "0$n";
      } else {
        return "00$n";
      }
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    String threeDigitMilliseconds =
        threeDigits(duration.inMilliseconds.remainder(1000));
    return "Осталось ${duration.inDays} дн. и ${twoDigits(duration.inHours.remainder(24))}:$twoDigitMinutes:$twoDigitSeconds:$threeDigitMilliseconds";
  }
}
