class Strings {
  static String myGoal = "Мои цели";
  static String createGoal = "Создать цель";
  static String tintNameGoal = "Название твоей цели";
  static String empty = "Тут пусто :(";
  static String enterGoalName = "Введите название цели";
  static String date = "Дата";
  static String save = "Сохранить";
  static String enterDate = "Выберите дату";
  static String timeToTargetCannotLessCurrent = "Время до цели не может быть меньше текущего";
}