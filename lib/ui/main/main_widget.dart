import 'package:flutter/material.dart';
import 'package:flutter_app/ui/main/list_item_widget.dart';
import 'package:flutter_app/ui/create_goal/create_goal_widget.dart';
import 'package:flutter_app/entities/goal.dart';

import 'dart:async' show Future;

import 'package:flutter_app/utils/strings.dart';

void main() async {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: Strings.myGoal,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: Strings.myGoal),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  String timeInWidget;
  List goals = <Goal>[];

  Future _createGoal() async {
    Goal results = await Navigator.of(context).push(new MaterialPageRoute<Goal>(
      builder: (BuildContext context) {
        return new CreateGoal();
      },
    ));
    if (results != null) {
      setState(() {
        goals.add(results);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: goals.isEmpty
          ? Center(child: Text(Strings.empty))
          : ListView(
              children: goals.map((timerView) => TimerView(timerView)).toList(),
            ),
      floatingActionButton: new FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _createGoal,
        tooltip: Strings.createGoal,
      ),
    );
  }
}
