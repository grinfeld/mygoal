import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/entities/goal.dart';
import 'package:flutter_app/utils/date_formatter.dart';

class TimerView extends StatefulWidget {
  const TimerView(this.goal);

  final Goal goal;

  @override
  TimerViewState createState() => TimerViewState();
}

class TimerViewState extends State<TimerView> {
  bool enabled = true;

  int time;
  StreamSubscription<int> listener;

  @override
  void initState() {
    super.initState();

    time = widget.goal.viewModel.duration;

    listener = widget.goal.viewModel.timeTicker.stream.listen((time) {
      setState(() {
        this.time = time;
      });
    }, onDone: () {
      setState(() {
        time = 0;
        enabled = false;
      });
    });
  }

  @override
  void dispose() {
    listener.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.goal.title),
      subtitle: Text(MyDateFormatter.getStringDuration(time)),
      enabled: enabled,
    );
  }
}
