import 'package:flutter/material.dart';
import 'package:flutter_app/entities/goal.dart';
import 'package:flutter_app/utils/strings.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter_app/view_model/timer_view_model.dart';

class CreateGoal extends StatefulWidget {
  @override
  _CreateGoalState createState() => _CreateGoalState();
}

class _CreateGoalState extends State<CreateGoal> {
  var formKey = GlobalKey<FormState>();
  String format = "dd-MM-yyyy HH:mm:ss";
  Goal goal = new Goal();
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.createGoal),
        ),
        body: Builder(
            builder: (context) => Container(
                  padding: EdgeInsets.all(15),
                  height: double.infinity,
                  child: SingleChildScrollView(
                    child: Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            onSaved: (s) => goal.title = s,
                            validator: (value) =>
                                value.isEmpty ? Strings.enterGoalName : null,
                            maxLength: 50,
                            textCapitalization: TextCapitalization.words,
                            keyboardType: TextInputType.text,
                            style: Theme.of(context).textTheme.title,
                            decoration: InputDecoration(
                              hintText: Strings.tintNameGoal,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDateTimePicker();
                            },
                            child: IgnorePointer(
                              child: new TextFormField(
                                controller: _controller,
                                decoration:
                                    new InputDecoration(hintText: Strings.date),
                                validator: (value) => dateValidator(value),
                                onSaved: (String val) {
                                  goal.viewModel = new TimerViewModel(
                                      getDurationGoalTimeStamp(val));
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          RaisedButton(
                            child: Text(Strings.save),
                            onPressed: () {
                              saveGoal();
                            }, //_saveGoal(context),
                          ),
                        ],
                      ),
                    ),
                  ),
                )));
  }

  void showDateTimePicker() {
    DatePicker.showDatePicker(
      context,
      minDateTime: DateTime.now(),
      initialDateTime: DateTime.now(),
      dateFormat: format,
      pickerMode: DateTimePickerMode.datetime,
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          var formatter = new DateFormat(format);
          _controller.text = formatter.format(dateTime);
        });
      },
    );
  }

  int getDurationGoalTimeStamp(String val) {
    final currentDate = DateTime.now();
    DateFormat format = new DateFormat(this.format);
    return format.parse(val).difference(currentDate).inMilliseconds;
  }

  void saveGoal() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      Navigator.of(context).pop(goal);
    }
  }

  dateValidator(String value) {
    if (value.isEmpty) {
      return Strings.enterDate;
    } else {
      final difference = getDurationGoalTimeStamp(value);
      if (difference < 0) {
        return Strings.timeToTargetCannotLessCurrent;
      } else {
        return null;
      }
    }
  }
}
