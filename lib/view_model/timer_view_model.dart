import 'dart:async';

class TimerViewModel {

  static const oneTick = const Duration(milliseconds: 50);

  TimerViewModel(this.duration) {
    _periodic = Timer.periodic(oneTick, tick);
  }

  void tick(_) {
    duration = duration - 50;
    timeTicker.add(duration);
    if (duration < 0) {
      _periodic.cancel();
      timeTicker.close();
    }
  }

  int duration;
  Timer _periodic;

  StreamController<int> timeTicker = StreamController<int>();
}
